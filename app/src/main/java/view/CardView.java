package view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import io.saul.martin.game.blade.android.simple.R;
import io.saul.martin.game.blade.card.PlayableCard;

public class CardView extends LinearLayout {
	private final PlayableCard card;
	private Integer graphic;
	private ImageView back;
	private ImageView front;
	private boolean alive;

	public CardView(Context context) {
		super(context);
		init();
		this.graphic = null;
		this.alive = false;
		card = null;
	}

	public CardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		this.graphic = null;
		this.alive = false;
		card = null;
	}

	public CardView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		this.graphic = null;
		this.alive = false;
		card = null;
	}

	public CardView(Context context, PlayableCard playableCard) {
		super(context);
		this.graphic = getGraph(playableCard.getSymbol());
		this.alive = playableCard.isActive();
		this.card = playableCard;
		init();
		front.setImageResource(graphic);
		update(playableCard);
	}

	private Integer getGraph(Character symbol) {
		switch (symbol){
			case '1': return R.mipmap.card_ac;
			case '2': return R.mipmap.card_2c;
			case '3': return R.mipmap.card_3c;
			case '4': return R.mipmap.card_4c;
			case '5': return R.mipmap.card_5c;
			case '6': return R.mipmap.card_6c;
			case '7': return R.mipmap.card_7c;
			case '8': return R.mipmap.card_8c;
			case '9': return R.mipmap.card_9c;
			case 'B': return R.mipmap.card_jc;
			case 'M': return R.mipmap.card_qc;
			case 'K': return R.mipmap.card_kc;
			case 'T': return R.mipmap.blue_back;
			default: return null;
		}
	}

	private void init() {
		inflate(getContext(), R.layout.view_card, this);
		this.back = findViewById(R.id.v_card_back);
		this.front = findViewById(R.id.v_card_front);
	}

	public void update(PlayableCard playableCard) {
		alive = playableCard.isActive();
		front.setVisibility(alive?VISIBLE:INVISIBLE);
		back.setVisibility(alive?INVISIBLE:VISIBLE);
	}

	public PlayableCard getCard() {
		return card;
	}
}
