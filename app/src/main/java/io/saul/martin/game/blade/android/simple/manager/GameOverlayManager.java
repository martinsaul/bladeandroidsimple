package io.saul.martin.game.blade.android.simple.manager;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GameOverlayManager {
	private final LinearLayout root;
	private final TextView textView;

	public GameOverlayManager(LinearLayout root, TextView textView) {
		this.root = root;
		this.textView = textView;
	}

	public void showOverlay(String message, int timeout) {
		textView.setText(message);
		root.setVisibility(View.VISIBLE);
		root.postDelayed(() -> {
			root.setVisibility(View.GONE);
		}, timeout);
	}
}
