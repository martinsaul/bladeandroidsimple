package io.saul.martin.game.blade.android.simple.manager;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import io.saul.martin.game.blade.android.simple.PlayerAction;
import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.listener.enumeration.VictoryCondition;
import view.CardView;

public class UIManager {

	private final Activity activity;
	private final GameOverlayManager gameOverlayManager;

	private HashMap<PlayableCard, CardView> opponentInPlayMap;
	private HashMap<PlayableCard, CardView> yourInPlayMap;
	private HashMap<PlayableCard, CardView> yourHandMap;

	private final LinearLayout opponentDeck;
	private final LinearLayout opponentHand;
	private final LinearLayout opponentInPlay;
	private final TextView opponentSum;

	private final LinearLayout yourDeck;
	private final LinearLayout yourHand;
	private final LinearLayout yourInPlay;
	private final TextView yourSum;
	private final PlayerAction playerAction;

	public UIManager(Activity activity, GameOverlayManager gameOverlayManager, LinearLayout opponentDeck, LinearLayout opponentHand, LinearLayout opponentInPlay, TextView opponentSum, LinearLayout yourDeck, LinearLayout yourHand, LinearLayout yourInPlay, TextView yourSum) {
		this.activity = activity;
		this.gameOverlayManager = gameOverlayManager;

		this.opponentInPlayMap = new HashMap<>();
		this.yourInPlayMap = new HashMap<>();
		this.yourHandMap = new HashMap<>();
		this.playerAction = new PlayerAction();

		this.opponentDeck = opponentDeck;
		this.opponentHand = opponentHand;
		this.opponentInPlay = opponentInPlay;
		this.opponentSum = opponentSum;
		this.yourDeck = yourDeck;
		this.yourHand = yourHand;
		this.yourInPlay = yourInPlay;
		this.yourSum = yourSum;
	}

	public void tie() {
		activity.runOnUiThread(() -> {
			gameOverlayManager.showOverlay("Tie!", 1000);
		});
	}

	public void setSum(TextView target, int value){
		target.setText(String.format(Locale.getDefault(),"%02d", value));
	}

	public void setOpponent(int opponentCardCount, int opponentSumOfCardsInPlay, ArrayList<PlayableCard> opponentCardsInPlay) {
		activity.runOnUiThread(() -> {
			setSum(opponentSum,opponentSumOfCardsInPlay);

			while(opponentCardCount > opponentHand.getChildCount())
				opponentHand.addView(new CardView(activity), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
			while (opponentCardCount < opponentHand.getChildCount())
				opponentHand.removeViewAt(0);

			validate(opponentInPlay, opponentInPlayMap, opponentCardsInPlay, null);
		});
	}

	private void validate(LinearLayout inPlayView, HashMap<PlayableCard, CardView> inPlayMap, ArrayList<PlayableCard> cardsInPlay, View.OnClickListener cardClick) {
		LinkedList<PlayableCard> existingCards = new LinkedList<>(inPlayMap.keySet());
		for(PlayableCard playableCard: cardsInPlay){
			existingCards.remove(playableCard);
			CardView cardView = inPlayMap.get(playableCard);
			if(cardView != null){
				cardView.update(playableCard);
			} else {
				cardView = new CardView(activity, playableCard);
				cardView.setOnClickListener(this.cardClick);
				inPlayMap.put(playableCard, cardView);
				inPlayView.addView(cardView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
			}
			cardView.setOnClickListener(cardClick);
		}
		while (!existingCards.isEmpty()){
			PlayableCard remaining = existingCards.pop();
			CardView cardView = inPlayMap.remove(remaining);
			inPlayView.removeView(cardView);
		}
	}

	public void setMyself(ArrayList<PlayableCard> myHand, int sumOfMyCardsInPlay, ArrayList<PlayableCard> myCardsInPlay) {
		activity.runOnUiThread(() -> {
			setSum(yourSum, sumOfMyCardsInPlay);

			validate(yourInPlay, yourInPlayMap, myCardsInPlay, null);
			validate(yourHand, yourHandMap, myHand, cardClick);
		});
	}

	public PlayerAction getPlayerAction() {
		return playerAction;
	}

	private View.OnClickListener cardClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			CardView cardView = (CardView) v;
			playerAction.setSelectedAction(cardView.getCard());
		}
	};

	public void victory(boolean victorious, VictoryCondition victoryCondition) {
		activity.runOnUiThread(() -> {
			gameOverlayManager.showOverlay("You " + (victorious?"win":"lose") + "!", 3000);
		});

	}
}
