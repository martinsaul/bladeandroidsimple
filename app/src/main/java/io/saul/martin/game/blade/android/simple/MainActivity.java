package io.saul.martin.game.blade.android.simple;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button crap = findViewById(R.id.play);
		crap.setOnClickListener(v -> {
			Intent intent = new Intent(MainActivity.this, GameActivity.class);
			startActivity(intent);
		});
	}
}
