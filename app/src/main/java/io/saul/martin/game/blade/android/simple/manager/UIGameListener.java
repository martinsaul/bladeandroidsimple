package io.saul.martin.game.blade.android.simple.manager;

import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.listener.GameListener;
import io.saul.martin.game.blade.listener.enumeration.VictoryCondition;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.state.GameState;
import io.saul.martin.game.blade.state.ImmutableState;
import io.saul.martin.game.blade.table.Seat;

public class UIGameListener extends GameListener {

	private final UIManager ui;

	public UIGameListener(UIManager ui, Player player) {
		super(player);
		this.ui = ui;
	}

	@Override
	public void tie() {
		ui.tie();
	}

	@Override
	public void winner(Seat winner, Seat loser, VictoryCondition victoryCondition) {
		ui.victory(winner.getPlayer() == getPlayer(), victoryCondition);
	}

	@Override
	public void cardPlayed(Seat seat, PlayableCard playableCard) {

	}

	@Override
	public void gameState(GameState gameState) {
		ImmutableState state = (ImmutableState) gameState;
		ui.setOpponent(state.getOpponentCardCount(), gameState.getOpponentSumOfCardsInPlay(), gameState.getOpponentCardsInPlay());
		ui.setMyself(state.getMyHand(), gameState.getSumOfMyCardsInPlay(), gameState.getMyCardsInPlay());
	}

	@Override
	public void playersTurn(Player player) {

	}
}
