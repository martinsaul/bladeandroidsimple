package io.saul.martin.game.blade.android.simple;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.saul.martin.game.blade.Game;
import io.saul.martin.game.blade.android.simple.manager.GameOverlayManager;
import io.saul.martin.game.blade.android.simple.manager.UIGameListener;
import io.saul.martin.game.blade.android.simple.manager.UIManager;
import io.saul.martin.game.blade.card.PlayableCard;
import io.saul.martin.game.blade.player.Player;
import io.saul.martin.game.blade.player.StandardPlayer;
import io.saul.martin.game.blade.state.MyGameState;

public class GameActivity extends AppCompatActivity {

	private UIManager uiManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		LinearLayout playerADeck = findViewById(R.id.game_playerA_deck);
		LinearLayout playerAHand = findViewById(R.id.game_playerA_hand);
		LinearLayout playerAInPlay = findViewById(R.id.game_playerA_inPlay);
		TextView playerASum = findViewById(R.id.game_playerA_sum);
		LinearLayout yourDeck = findViewById(R.id.game_you_deck);
		LinearLayout yourHand = findViewById(R.id.game_you_hand);
		LinearLayout yourInPlay = findViewById(R.id.game_you_inPlay);
		TextView yourSum = findViewById(R.id.game_you_sum);

		uiManager = new UIManager(
				this,
				new GameOverlayManager(findViewById(R.id.game_overlay_root), findViewById(R.id.game_overlay_text)),
				playerADeck,
				playerAHand,
				playerAInPlay,
				playerASum,
				yourDeck,
				yourHand,
				yourInPlay,
				yourSum
		);
		Player player = new Player() {
			@Override
			public PlayableCard move(MyGameState myGameState) {
				return uiManager.getPlayerAction().getActionCard();
			}
		};
		UIGameListener uiGameListener = new UIGameListener(uiManager, player);

		Game game = new Game(player, new StandardPlayer());
		game.registerGameListener(uiGameListener);

		new Thread(game::gameFlow).start();
	}
}
