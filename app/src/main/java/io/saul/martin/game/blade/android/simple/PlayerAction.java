package io.saul.martin.game.blade.android.simple;

import io.saul.martin.game.blade.card.PlayableCard;

public class PlayerAction {
	private PlayableCard selectedAction;
	private Object lock = new Object();

	public void setSelectedAction(PlayableCard card){
		synchronized(lock) {
			this.selectedAction = card;
			lock.notify();
		}
	}

	public PlayableCard getActionCard() {
		synchronized(lock) {
			try {
				lock.wait();
				return selectedAction;
			} catch (InterruptedException e) {
				throw new RuntimeException("Interruption happened when waiting for player move.", e);
			}
		}
	}
}
